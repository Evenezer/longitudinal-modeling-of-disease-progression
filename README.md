# Preprocessing
## Aim
#### Perform the necessary steps for data preparation of cohorts A and B prior to data modeling.

#### General preprocessing steps
Semantic understanding of the data features and samples.
De-duplication of samples
Infer the follow-up features based on the baseline features/measurements
Deal with outliers & missing data.
Data transformation
One-hot encoding of categorical data
Dimensionality reduction
# Summary Statistic
## Aim
#### Generate a table with summary statistics of your features within cohorts A and B.

General entries in a summary statistics table
Categorical data: Counts, Percentages, Frequencies, & Proportions
Numerical data: Mean, Variance, Standard Deviation, Median, MAD​, Mode, IQR, & Range
Correlation: Pearson, Spearman, Kendal
Covariance
# Visualization
## Aim
#### Select appropriate visualization techniques for your features in cohorts A and B.

#### General visualization techniques
Categorical data: Pie Chart and Bar Plot.
Numerical data:  Violin Plot and Histogram.
# Longitudinal Modeling
## Aim
We would like to see the biological patterns that exist within cohorts A and B. Would the patterns look exactly the same among these two populations given that the underlying patients belong to different racial groups?

#### General longitudinal modeling methods
Multi-state modeling of longitudinal data to extract the probability of transitioning between the disease states
Prediction/Simulation of states diagram for new samples
Some python libraries - pymsm, scikit-survival
#### Note: For further information, you can read the following paper https://alz-journals.onlinelibrary.wiley.com/doi/full/10.1002/alz.12387